rc_msgs
=======

__Custom messages for the Robust RC project.__

This is a simple package containing only shared message definitions. This entire description is stolen from https://bitbucket.org/kandidatrc/rc_msgs/, 
who taught us how to do this type of thing.

-------

### StateStamped
Provides the full state of the vehicle.

### ControlSignal
Provides the control signal to the car.

-------

## Howto

* Create MSG package with `message_generation` and `std_msgs` as dependencies.
* Uncomment `add_message_files` in CMakeList.txt

```
add_message_files(
    FILES
    CustomMessage.msg
)
```
* Uncomment `generate_messages` in CMakeList.txt

```
generate_messages(
    DEPENDENCIES
    std_msgs
)
```
* Ensure `CATKIN_DEPENDS` in `catkin_package` is followed by `message_runtime` and `std_msgs` and is uncommented.
* Make sure dependencies are in `package.xml` as well (see this repository for example).
* In the library you wish to use the messages in, make sure `rc_msgs` (or your library) is added in `find_package` as well as `CATKIN_DEPENDS` line in `catkin_package`. When in doubt, check `include_directories` so it includes `${catkin_INCLUDE_DIRS}`.
* You should now be good to include headers like, `#include <rc_msgs/PoseStamped.h>`.
